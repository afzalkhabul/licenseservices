var server = require('../../server/server');
module.exports = function(Hearingdate) {
	Hearingdate.observe('before save', function (ctx, next) {
    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();
 var createCase = server.models.CreateCase;
 createCase.find({"where":{"caseNumber":ctx.instance.caseNumber}},function (err, CreateCase) {
var caseSubDate = CreateCase[0].caseDate;
var caseSplit = caseSubDate.split('-');
var finalCase = caseSplit[1]+"/"+caseSplit[0]+"/"+caseSplit[2];
var finalCaseDate = new Date(finalCase)
var caseFinalDate = finalCaseDate.getTime();
var hearingSubDate = ctx.instance.dateHearing;
var hearingSplit = hearingSubDate.split('-');
var finalHearing = hearingSplit[1]+"/"+hearingSplit[0]+"/"+hearingSplit[2];
var finalHearingDate = new Date(finalHearing)
var hearingFinalDate = finalHearingDate.getTime();

if(caseFinalDate<hearingFinalDate){
  Hearingdate.find({"where":{"caseNumber":ctx.instance.caseNumber,"dateHearing":ctx.instance.dateHearing}},function (err, hearing) {
      if(hearing.length>0){
      var err = new Error("Hearing date for same case could be unique");
         next(err);
      }else{
        next();
      }
  });

  }
else{
var error = new Error("Hearing Date should be greather than Case Submission Date");
   next(error);
next();
}
  });
    }else {
      next();
    }
  });

  Hearingdate.observe('after save', function (ctx, next) {
    var Hearingdate = server.models.Hearingdate;
    if(ctx.isNewInstance){
      var hearingStatus = ctx.instance.status;
      var createCase = server.models.CreateCase;
      var judgement = server.models. Judgement;
      var Sms = server.models.Sms;
      var Emailtemplete = server.models.EmailTemplete;
      createCase.find({"where":{"caseNumber":ctx.instance.caseNumber}},function (err, CreateCase) {
        CreateCase[0].updateAttributes({status : hearingStatus},function(err,sucuss){
        })
        judgement.find({"where":{"caseNumber":ctx.instance.caseNumber}, order:'judgementDate DESC'},function (err, Judgement) {
          if (Judgement.length > 0) {
            Judgement[0].updateAttributes({status: hearingStatus}, function (err, sucuss) {
            })
          }
          Emailtemplete.find({"where": {"emailType": "legal"}}, function (err, legalemailtemplate) {
            var Dhanbademail = server.models.DhanbadEmail;
            for (var i = 0; i < ctx.instance.advocateName.length; i++) {
              Dhanbademail.create({
                "to": ctx.instance.advocateName[i].email,
                "subject": legalemailtemplate[0].hearingSubject,
                "text": "Details : For Case Number : " + ctx.instance.caseNumber + " Hearing Date is :" + ctx.instance.dateHearing + legalemailtemplate[0].hearingText
              }, function (err, email) {
              });
              if(legalemailtemplate[0].hearingSMS && ctx.instance.advocateName[0].phone) {
                var smsData = {
                  "message": legalemailtemplate[0].hearingSMS,
                  "mobileNo": ctx.instance.advocateName[0].phone,
                  "smsservicetype": "singlemsg"
                };
                Sms.create(smsData, function (err, smsInfo) {
                });
              }
            }
            next();
          });
        });
      });


    }

    else {
      next();
    }

  });

    Hearingdate.observe('loaded', function(ctx, next) {
      if(ctx.instance){
        var Advocate = server.models.Advocate;
        var advocateName=ctx.instance.advocateName;
        ctx.instance['advocateList']=[];
        if(advocateName!=null && advocateName.length>0){
          var count=0;
          for(var i=0;i<advocateName.length;i++){
            var advocateId=advocateName[i].advId;
            Advocate.find({'where':{'advId':advocateId}}, function (err, advocateList) {
              count++;
              if(advocateList!=null && advocateList.length>0){
                var advocateDetails=advocateList[0]
                ctx.instance.advocateList.push({
                  'name':advocateDetails.name,
                  'email':advocateDetails.email,
                  'advId':advocateDetails.advId
                });
              }
             if(count==advocateName.length){
                next();
             }
            });
          }
        }else{
          next();
        }
      }else{
        next();
      }

    });

};
