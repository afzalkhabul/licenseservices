var server = require('../../server/server');
module.exports = function(Projectplan) {
  Projectplan.observe('before save', function (ctx, next) {

    var WorkflowForm=server.models.WorkflowForm;
    var Workflow=server.models.Workflow;
    var WorkflowEmployees=server.models.WorkflowEmployees;
    var notification=server.models.notification;
    if(ctx.instance){
        WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "projectPlan"}]}}, function (err, workflowForm) {
          if(workflowForm!=undefined && workflowForm!=null ){
            Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
              WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
                var workflowData = [];
              var employeeDetailsForNotification=[];
                if (listData != undefined && listData != null && listData.length > 0) {
                  for (var i = 0; i < listData.length; i++) {
                    var flowData = {
                      workflowId: listData[i].workflowId,
                      levelId: listData[i].levelId,
                      status: listData[i].status,
                      maxLevels: listData[i].maxLevels,
                      levelNo: listData[i].levelNo
                    }
                    workflowData.push(flowData);
                    if(listData[i].levelNo=="1"){
                      if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                        for(var j=0;j<listData[i].employees.length;j++){
                          employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                        }
                      }
                    }
                  }
                }
                ctx.instance.workflow = workflowData;
                ctx.instance.workflowId=workflowForm.workflowId;
                ctx.instance.maxlevel=flowDataList.maxLevel;
                ctx.instance.acceptLevel=0;
                ctx.instance.finalStatus=false;
                ctx.instance.createdTime=new Date();
                ctx.instance.lastUpdateTime=new Date();
                if(employeeDetailsForNotification.length>0){
                  notification.create({
                    'to':employeeDetailsForNotification,
                    "subject":  'New Tasks',
                    "text":"You got New Plan Request",
                    "message":"You got New Plan Request",
                    'urlData':'projectPlanDetails',
                    "type":"newRequest"
                  }, function (err, notificationDetails){
                    next();
                  });

                }else{
                  next();
                }


              });

            });
          }else{
            next();
          }
        });

      }else{
        next();
      }

  });

  Projectplan.observe('loaded', function(ctx, next) {
    var TaskStatus=server.models.TaskStatus;
    var ProjectState=server.models.ProjectState;
    if(ctx.instance) {
      if (ctx.instance.taskStatusId) {
        TaskStatus.findById(ctx.instance.taskStatusId, function (err, statusInfo) {
          if (err) {
            next(err, null);
          } else {
            ctx.instance['taskStatus'] = statusInfo.name;
            if (ctx.instance.palnStatusId) {
              ProjectState.findById(ctx.instance.palnStatusId, function (err, status) {
                if (err) {
                  next(err, null);
                } else {
                  ctx.instance['palnStatus'] = status.name;
                  if (ctx.instance.departmentId) {
                    var Department = server.models.PlanDepartment;
                    Department.findById(ctx.instance.departmentId, function (err, deptData) {
                      if (err) {

                      } else {
                        ctx.instance['department'] = deptData.name;
                      }
                      if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
                        var WorkflowEmployees = server.models.WorkflowEmployees;
                        WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
                          if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
                            ctx.instance.workflowData = workflowEmployeeList;
                            next();
                          } else {
                            next();
                          }
                        })
                      }
                      else {
                        next();
                      }
                    });
                  }else{
                    if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
                      var WorkflowEmployees = server.models.WorkflowEmployees;
                      WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
                        if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
                          ctx.instance.workflowData = workflowEmployeeList;
                          next();
                        } else {
                          next();
                        }
                      })
                    }
                    else {
                      next();
                    }
                  }
                }
              });
            }else{
              if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
                var WorkflowEmployees = server.models.WorkflowEmployees;
                WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
                  if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
                    ctx.instance.workflowData = workflowEmployeeList;
                    next();
                  } else {
                    next();
                  }
                })
              }
              else {
                next();
              }
            }

          }
        });
      }else if (ctx.instance.palnStatusId) {
        ProjectState.findById(ctx.instance.palnStatusId, function (err, status) {
          if (err) {
            next(err, null);
          } else {
            ctx.instance['palnStatus'] = status.name;
            if (ctx.instance.departmentId) {
              var Department = server.models.PlanDepartment;
              Department.findById(ctx.instance.departmentId, function (err, deptData) {
                if (err) {

                } else {
                  ctx.instance['department'] = deptData.name;
                }
                next();
              });
            }else{
              next();
            }
          }
        });
      } else  if (ctx.instance.departmentId) {
        var Department = server.models.PlanDepartments;
        Department.findById(ctx.instance.departmentId, function (err, deptData) {
          if (err) {

          } else {
            ctx.instance['department'] = deptData.name;
          }
          next();
        });
      }else{
        next();
      }
    } else {
      next();
    }
  });


  Projectplan.getDetailsForWorkFlow = function (employee, cb) {
    Projectplan.find({},function(err, fieldVisitData){
      var fieldVisitList=[];
      var adminStatus=false;
      if(fieldVisitData!=null && fieldVisitData.length>0){
        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            } else {
            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }
                    }
                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }
              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });
      }
      else{
        cb(null,fieldVisitList);
      }
    });
  };


  Projectplan.remoteMethod('getDetailsForWorkFlow', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetailsForWorkFlow',
      verb: 'GET'
    }
  });



  Projectplan.updateDetails = function (approvalDetails, cb) {
    Projectplan.findOne({'where':{'planId':approvalDetails.planId  }},function(err, fieldVisitDetails){
      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        if(fieldVisitDetails.acceptLevel!=undefined && fieldVisitDetails.acceptLevel!=null && fieldVisitDetails.acceptLevel==approvalDetails.acceptLevel){
         var workflowDetails=fieldVisitDetails.workflow;
          if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
            var workflowData=[];
            for(var  i=0;i< workflowDetails.length;i++){
              var data=workflowDetails[i];
              var workFlowLevel=parseInt(workflowDetails[i].levelNo);
              if((approvalDetails.acceptLevel+1)==workFlowLevel){
                data.approvalStatus=approvalDetails.requestStatus;
                data.employeeName=approvalDetails.employeeName;
                data.employeeId=approvalDetails.employeeId;
                data.comment=approvalDetails.comment;
                data.date=new Date();
              }
              workflowData.push(data);
            }
            finalObject.workflow=workflowData;
            finalObject.lastUpdateTime=new Date();
          }
          if(approvalDetails.requestStatus=="Approval"){
            if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
              finalObject.finalStatus=approvalDetails.requestStatus;
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }else{
              finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            }
          }
          else if(approvalDetails.requestStatus=="Rejected"){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
            finalObject.rejectComment=approvalDetails.comment;
          }
          fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){

            var Sms = server.models.Sms;
            if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
              if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){
                    EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                      if(emailTemplete!=null ) {
                        var message = emailTemplete.projectPlanRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project plan Request is Rejected",
                          "message":"Your project plan Request is Rejected",
                          'urlData':'projectPlanDetails',
                          "type":"approvedRequest"
                        }, function (err, notificationDetails){
                        });
                        Dhanbademail.create({
                          "to": employeeDetails[0].emailId,
                          "subject":  emailTemplete.projectPlanRejectedEmail,
                          "text":message
                        }, function (err, email) {
                        });

                        if(emailTemplete.docUploadRejectedSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.projectPlanRejectedSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };

                          Sms.create(smsData, function (err, smsInfo) {

                          });
                        }
                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });

              }else{
                cb(null, finalData);
              }

            }
            else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){

              if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                var Employee=server.models.Employee;
                var EmailTemplete = server.models.EmailTemplete;
                Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                  if(employeeDetails!=null && employeeDetails.length>0){
                    EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                      if(emailTemplete!=null) {
                        var message = emailTemplete.projectPlanRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                        var Dhanbademail = server.models.DhanbadEmail;
                        var notification=server.models.notification;
                        notification.create({
                          'to':[{'employeeId':employeeDetails[0].employeeId,'readStatus':false}],
                          "subject":  'New Tasks',
                          "text":"Your project plan Request is approved",
                          "message":"Your project plan Request is approved",
                          'urlData':'projectPlanDetails',
                          "type":"approvedRequest"
                        }, function (err, notificationDetails){
                        });
                        Dhanbademail.create({
                          from: emailTemplete.emailId,
                          to: employeeDetails[0].email,
                          subject: emailTemplete.projectPlanRequestApprovalEmail, // Subject line
                          "text":message
                        }, function (err, email) {

                        });

                        if(emailTemplete.projectPlanRequestApprovalSMS && employeeDetails[0].mobile) {
                          var Sms = server.models.Sms;
                          var smsData = {
                            "message": emailTemplete.projectPlanRequestApprovalSMS,
                            "mobileNo": employeeDetails[0].mobile,
                            "smsservicetype": "singlemsg"
                          };

                          Sms.create(smsData, function (err, smsInfo) {
                            console.log('SMS info:' + JSON.stringify(smsInfo));
                          });
                        }

                      }
                    });
                    cb(null, finalData);
                  }else{
                    cb(null, finalData);
                  }
                });
              }
            }else{
              if(finalData.workflowId!=undefined && finalData.workflowId!=null && finalData.workflowId!=''){
                var WorkflowEmployees=server.models.WorkflowEmployees;
                var notification=server.models.notification;
                WorkflowEmployees.find({'where':{'workflowId':finalData.workflowId}},function (err, listData) {
                  var workflowData = [];
                  var employeeDetailsForNotification=[];
                  if (listData != undefined && listData != null && listData.length > 0) {
                    for (var i = 0; i < listData.length; i++) {
                      if(parseInt(listData[i].levelNo)==(finalData.acceptLevel+1)){
                        if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                          for(var j=0;j<listData[i].employees.length;j++){
                            employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                          }
                        }
                      }
                    }
                  }
                  if(employeeDetailsForNotification.length>0){
                    notification.create({
                      'to':employeeDetailsForNotification,
                      "subject":  'New Tasks',
                      "text":"You got New Plan Request",
                      "message":"You got New Plan Request",
                      'urlData':'projectPlanDetails',
                      "type":"newRequest"
                    }, function (err, notificationDetails){
                      cb(null, finalData);
                    });

                  }else{
                    cb(null, finalData);
                  }
                });
              }else{
                cb(null, finalData);
              }

            }

          });
        }
        else{
          var error = new Error('This level is already approved. Can you please refresh page');
          error.statusCode = 200;
          cb(error,null);
        }

      }else {
        var error = new Error('Your plan details does not exits');
        error.statusCode = 200;
        cb(error,null);
      }

    });
  };

  Projectplan.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Projectplan.updateContent= function (updatedDetails, cb) {

    Projectplan.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      var notification=server.models.notification;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            var employeeDetailsForNotification=[];
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  }
                  workflowData.push(flowData);
                  if(listData[i].levelNo=="1"){
                    if(listData[i].employees!=undefined && listData[i].employees!=null && listData[i].employees.length>0){
                      for(var j=0;j<listData[i].employees.length;j++){
                        employeeDetailsForNotification.push({'employeeId':listData[i].employees[j],'readStatus':false})
                      }
                    }
                  }
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails.lastUpdateTime=new Date();
              updatedDetails['updatedTime']=new Date();
              notification.create({
                'to':employeeDetailsForNotification,
                "subject":  'New Tasks',
                "text":"You got edit Plan Request",
                "message":"You got edit Plan Request",
                'urlData':'projectPlanDetails',
                "type":"newRequest"
              }, function (err, notificationDetails){
              console.log('after update new plan request created');
              });
              fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
                console.log('successfully updated');
              });
              cb(null, fieldVisitDetails);
            });

          });
        }else{
          var error = new Error('Please check workflow details');
          error.statusCode = 200;

          cb(error,null);
        }
      });




    });
  };

  Projectplan.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });



  Projectplan.getDetails = function (planId, cb) {
    var ProjectTasks = server.models.ProjectTasks;
    var ProjectComments = server.models.ProjectComments;
    var ProjectFiledVisit = server.models.FieldVisit;
    var Projectuploads = server.models.ProjectUploads;
    var Paymentmilestone = server.models.PaymentMilestone;
    var Projectrisks = server.models.ProjectRisks;
    var Projectcharter = server.models.ProjectCharter;
    var PlanLayout = server.models.PlanLayout;
    var TransferFund = server.models.TransferFund;
    var ProjectDeliverable = server.models.ProjectDeliverable;
    var ProjectMilestone = server.models.ProjectMilestone;
    var ProjectRisks = server.models.ProjectRisks;
    var ProjectIssues = server.models.ProjectIssues;
    var ProjectRebaseLine= server.models.ProjectRebaseline;
    var ProjectMeasurement= server.models.ProjectMeasurement;
    var ProjectBillGeneration= server.models.BillGeneration;
    var ProjectPaymentOrder= server.models.ProjectPaymentOrder;
    var ProjectWorkOrder= server.models.ProjectWorkOrder;
    var ProjectClosure = server.models.ProjectClosure;
    console.log('data is ' + JSON.stringify(planId));
    var details = planId;
    var planDetails = {
      'planDetails': [],
      'taskDetails': [],
      'comments': [],
      'filedVisit': [],
      'uploads': [],
      'paymentMilestones': [],
      'projectRisks': [],
      'projectCharter': [],
      'projectLayoutUploads': [],
      'projectTransferFunds': [],
      'projectDeliverable': [],
      'projectMilestone': [],
      'projectIssues': [],
      'projectMeasurement': [],
      'projectBillGeneration': [],
      'projectPaymentOrder': [],
      'projectWorkOrder': [],
      'projectRebaseLine': [],
      'projectClosure': []
    };

    Projectplan.find({'where': {'planId': details.planId}}, function (err, planDetailsList) {
      if (err)
        console.log('Error plan message  is' + err);

      planDetails['planDetails'] = planDetailsList;
      Projectuploads.find({"where": {"and": [{"type": "layout"}, {"planId": details.planId}]}}, function (err, projectLayoutUploads) {
        if (err)
          console.log('Error Project uploads message is' + err);

        planDetails['projectLayoutUploads'] = projectLayoutUploads;

        TransferFund.find({'where': {'planId': details.planId}}, function (err, projectTransferFunds) {
          if (err)
            console.log('Error transfer message is' + err);

          planDetails['projectTransferFunds'] = projectTransferFunds;
          ProjectDeliverable.find({'where': {'planId': details.planId}}, function (err, projectDeliverable) {
            if (err)
              console.log('Error deliverables message is' + err);
            planDetails['projectDeliverable'] = projectDeliverable;

            ProjectMilestone.find({'where': {'planId': details.planId}}, function (err, projectMilestone) {
              if (err)
                console.log('Error milestones message is' + err);
              planDetails['projectMilestone'] = projectMilestone;
              ProjectRisks.find({'where': {'planId': details.planId}}, function (err, projectRisks) {
                if (err)
                  console.log('Error risks message is' + err);
                planDetails['projectRisks'] = projectRisks;

                ProjectClosure.find({'where': {'planId': details.planId}}, function (err, projectClosure) {
                  if (err)
                    console.log('Error closure message is' + err);
                  planDetails['projectClosure'] = projectClosure;
                  ProjectTasks.find({'where': {'planId': details.planId}}, function (err, taskDetails) {
                    if (err)
                      console.log('Error tasks message is' + err);
                    planDetails['taskDetails'] = taskDetails;

                    ProjectComments.find({'where': {'planId': details.planId}}, function (err, projectComments) {
                      if (err)
                        console.log('Error message is' + err);
                      planDetails['comments'] = projectComments;

                      ProjectFiledVisit.find({'where': {'planId': details.planId}}, function (err, projectFiledVisit) {
                        if (err)
                          console.log('Error filed visit message is' + err);
                        planDetails['filedVisit'] = projectFiledVisit;

                        Projectuploads.find({"where": {"and": [{"type": "upload"}, {"planId": details.planId}]}}, function (err, projectuploads) {
                          if (err)
                            console.log('Error uploads message is' + err);
                          planDetails['uploads'] = projectuploads;
                          Paymentmilestone.find({'where': {'planId': details.planId}}, function (err, paymentmilestone) {
                            if (err)
                              console.log('Error payment milestone message is' + err);
                            planDetails['paymentMilestones'] = paymentmilestone;
                            Projectrisks.find({'where': {'planId': details.planId}}, function (err, projectrisks) {
                              if (err)
                                console.log('Error risks message is' + err);
                              planDetails['projectRisks'] = projectrisks;
                              Projectcharter.find({'where': {'projectId': details.planId}}, function (err, projectcharter) {
                                if (err)
                                  console.log('Error message is' + err);
                                planDetails['projectCharter'] = projectcharter;
                              ProjectIssues.find({'where': {'planId': details.planId}}, function (err, projectIssues) {
                                if (err)
                                  console.log('Error risks message is' + err);
                                planDetails['projectIssues'] = projectIssues;
                                ProjectRebaseLine.find({'where':{'and': [{'planId': details.planId},{ "finalStatus" : "Approval"}]}}, function (err,rebaseLine) {
                                  if(err)
                                  console.log('Error message in rebase line'+err);
                                  planDetails['projectRebaseLine']=rebaseLine;
                                 ProjectMeasurement.find({'where': {'planId': details.planId}}, function (err, projectMesurementDetails) {
                                   if(err)
                                     console.log('Error message in measurement'+err);
                                   planDetails['projectMeasurement']=projectMesurementDetails;
                                   ProjectBillGeneration.find({'where': {'planId': details.planId}}, function (err, projectBillgeneration) {
                                     if(err)
                                       console.log('Error message in bill generation'+err);
                                     planDetails['projectBillGeneration']=projectBillgeneration;
                                     ProjectPaymentOrder.find({'where': {'planId': details.planId}}, function (err, projectPaymentOrder) {
                                       if(err)
                                         console.log('Error message in '+err);
                                       planDetails['projectPaymentOrder']=projectPaymentOrder;
                                       ProjectWorkOrder.find({'where': {'planId': details.planId}}, function (err, projectWorkOrder) {
                                         if(err)
                                           console.log('Error message in Project work order'+err);
                                         planDetails['projectWorkOrder']=projectWorkOrder;
                                         cb(null, planDetails);
                                       });
                                     });
                                   });
                                 });
                                });
                              });
                              });
                            });
                          });
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });


      });


    });
  }

  Projectplan.remoteMethod('getDetails', {
    description: "Send Valid PlanId",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'planId', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });

  Projectplan.getProjectDetails = function (planId, cb) {
        var TaskStatus=server.models.TaskStatus;
        TaskStatus.find({},function(err, taskDetails){
          if(taskDetails!=null && taskDetails.length>0){
            var condition=[];
            for(var i=0;i<taskDetails.length;i++){
              if(taskDetails[i].name=="new" ||taskDetails[i].name=="pending" || taskDetails[i].name=="working"|| taskDetails[i].name=="completed"){
                var details={
                  'taskStatusId':taskDetails[i].id+''
                }
                condition.push(details);
              }
            }
            if(condition.length>0){
              Projectplan.find({'where':{'and':[{'or':condition},{"finalStatus" : "Approval"}]} },function(err, projectpalnDetails){
                cb(null,projectpalnDetails);
              })
            }else{
              var details=[];
              cb(null,details);
            }
          } else{
            var details=[];
            cb(null,details);
          }
        });
      }

  Projectplan.remoteMethod('getProjectDetails', {
        description: "Send Valid PlanId",
        returns: {
          arg: 'data',
          type: "object",
          root:true
        },
        accepts: [{arg: 'planId', type: 'object', http: {source: 'query'}}],
        http: {
          path: '/getProjectDetails',
          verb: 'GET'
        }
      });

  Projectplan.sendAnEmailPropertyTag = function (planId, cb) {

    var appConfigDetails = server.models.AppConfig;
    appConfigDetails.find({}, function(err, appConfigDetails) {
        if(appConfigDetails != undefined && appConfigDetails != null && appConfigDetails.length>0) {
          var appConfigObj = appConfigDetails[0];
            if(appConfigObj.emailText != undefined && appConfigObj.emailText != null && appConfigObj.emailText != '') {
              Projectplan.findOne({'where':{'planId':planId }},function(err, projectDetails){
                if(projectDetails != undefined && projectDetails !=null && projectDetails != '') {
                var Dhanbademail = server.models.DhanbadEmail;

                  var planDetailsText = "<html><body><div>Your Project Details are" + "</div><hr />" +
                                        "<table><tbody><tr><td>Plan Id</td><td>:</td><td>" + projectDetails.planId + "</td></tr>" +
                                        "<tr><td>Plan Name</td><td>:</td><td>" + projectDetails.name + "</td></tr>" +
                                        "<tr><td>Description</td><td>:</td><td>" + projectDetails.description + "</td></tr>" +
                                        "<tr><td>ULB</td><td>:</td><td>" + projectDetails.ulb + "</td></tr>" +
                                        "<tr><td>Applicant Id</td><td>:</td><td>" + projectDetails.applicantId + "</td></tr>" +
                                        "<tr><td>Task StatusId</td><td>:</td><td>" + projectDetails.taskStatusId + "</td></tr>" +
                                        "<tr><td>Paln StatusId</td><td>:</td><td>" + projectDetails.palnStatusId + "</td></tr>" +
                                        "<tr><td>Start Date</td><td>:</td><td>" + projectDetails.startDate + "</td></tr>" +
                                        "<tr><td>End Date</td><td>:</td><td>" + projectDetails.endDate + "</td></tr>" +
                                        "<tr><td>Estimation Cost</td><td>:</td><td>" + projectDetails.estimationCost + "</td></tr>" +
                                        "<tr><td>Utilized Cost</td><td>:</td><td>" + projectDetails.utilizedCost + "</td></tr>" +
                                        "<tr><td>Financial Year</td><td>:</td><td>" + projectDetails.financialYear + "</td></tr>" +
                                        "<tr><td>Village</td><td>:</td><td>" + projectDetails.village + "</td></tr>" +
                                        "<tr><td>Mandal</td><td>:</td><td>" + projectDetails.mandal + "</td></tr>" +
                                        "<tr><td>City</td><td>:</td><td>" + projectDetails.city + "</td></tr>" +
                                        "<tr><td>Address</td><td>:</td><td>" + projectDetails.address + "</td></tr>" +
                                        "<tr><td>Complete Percentage</td><td>:</td><td>" + projectDetails.completePercentage + "</td></tr>" +
                                        "<tr><td>Task Status</td><td>:</td><td>" + projectDetails.taskStatus + "</td></tr>" +
                                        "<tr><td>Paln Status</td><td>:</td><td>" + projectDetails.palnStatus + "</td></tr>" +
                                        "<tr><td>Department</td><td>:</td><td>" + projectDetails.department + "</td></tr>" +
                                        "<tr><td>Department Id</td><td>:</td><td>" + projectDetails.departmentId + "</td></tr>" +
                                        "</tbody><body></table>";

                                          Dhanbademail.create({
                                            "to": appConfigDetails[0].emailText,
                                            "subject": "Project Details for property tags",
                                            "text": planDetailsText
                                          }, function (err, email) {
                                            //console.log(email);
                                          });


                }
              });
            } else {

            }
          }
        })
    }

    Projectplan.remoteMethod('sendAnEmailPropertyTag', {
          description: "Send Valid PlanId",
          returns: {
            arg: 'data',
            type: "object",
            root:true
          },
          accepts: [{arg: 'planId', type: 'string', http: {source: 'query'}}],
          http: {
            path: '/sendAnEmailPropertyTag',
            verb: 'GET'
          }
        });


  Projectplan.observe('after save', function (ctx, next) {

    if(ctx.isNewInstance){
        next();

    }else{
      next();
    }

  });



}


