var server = require('../../server/server');
module.exports = function(AssetForLand) {


    AssetForLand.observe('before save', function (ctx, next) {

    if(ctx.instance!=undefined && ctx.instance!=null){
      ctx.instance.createdTime = new Date();
      var randomstring = require("randomstring");
      var landType = ctx.instance.assetType.toUpperCase();
       var locality =ctx.instance.locality.toUpperCase();
      var assetNumber = randomstring.generate({
        length: 6,
        charset: 'alphanumeric'
      });
      ctx.instance['assetNumber'] = landType.substring(0,4)+"_"+locality.substring(0,3)+"_"+assetNumber.toUpperCase();
    if(ctx.instance.projectDetails!=undefined && ctx.instance.projectDetails
        && ctx.instance.projectDetails.projectCharter!=undefined && ctx.instance.projectDetails.projectCharter && ctx.instance.projectDetails.projectCharter!='' ){

        var Projectcharter = server.models.ProjectCharter;
        Projectcharter.findOne({'where':{'charterUniqueId':ctx.instance.projectDetails.projectCharter}}, function (err, charterDetails) {
          if(charterDetails!=null){
            var charter={
              'name':charterDetails.name,
              'description':charterDetails.description,
              'createdPerson':charterDetails.createdPerson,
              'status':'new',
              'charterUniqueId':charterDetails.charterUniqueId
            };
            Projectcharter.create(charter, function (err, charterData) {
              if(charterData!=null){
                var TaskStatus=server.models.TaskStatus;
                var ProjectPlan=server.models.ProjectPlan;
                var details={};
                TaskStatus.findOne({'where':{'name':'new'}},function(err, taskDetails){
                 if(taskDetails!=null){
                   details={
                     "planId": charterData.projectId,
                     "name": ctx.instance.projectDetails.projectPlan,
                     "departmentId": ctx.instance.projectDetails.departmentInfo,
                     "financialYear": ctx.instance.projectDetails.financialYear,
                     'taskStatusId':taskDetails.id
                   }
                   ProjectPlan.create(details, function (err, planDetails) {
                     if(planDetails!=null){
                       ctx.instance['planId']=planDetails.planId;
                       next();
                     }else{
                       next();
                     }
                   });
                 }else{
                   details={
                     "planId": charterData.projectId,
                     "name": ctx.instance.projectDetails.projectPlan,
                     "departmentId": ctx.instance.projectDetails.departmentInfo,
                     "financialYear": ctx.instance.projectDetails.financialYear,
                   }
                   ProjectPlan.create(details, function (err, planDetails) {
                     if(planDetails!=null){
                       ctx.instance['planId']=planDetails.planId;
                       next();
                     }else{
                       next();
                     }
                   });
                 }

                });
              }else{
                next();
              }

            });

          }else{
            next();
          }

        });


      }
      else{
        next();
      }
    }else {

      ctx.data.updatedTime = new Date();
      next();

    }
  });


  AssetForLand.remoteMethod('importAssetData', {
    description: "Send Valid Details",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'someData', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/importAssetData',
      verb: 'POST'
    }
  });

    AssetForLand.importAssetData = function (data, cb) {
    var url=data[0].id;
    var fs=require('fs');
    var accountNumber=[];
    var AccountList=[];
    var assetName=[];
    var AssetTypeList=[];
    var department=[];
    var DepartmentList=[];
    var assetStatus=[];
    var AssetStatusList=[];
    var costCenter=[];
    var CostCenterList=[];
    var assetSpare=[];
    var AssetSparesList=[];

    var AccountDetails = server.models.accountDetails;
    var DepartmentDetails = server.models.AssetDepartments;
    var AssetTypeDetails = server.models.AssetType;
    var AssetStatusDetails = server.models.AssetStatus;
    var CostCenterDetails = server.models.CostCenter;
    var AssetSparesDetails = server.models.AssetSpares;
    var TypeOfDuties=server.models.TypeOfDuty;

    var request = require('request');
    var result= request('http://localhost:8869/api/Uploads/dhanbadDb/download/'+url).pipe(
      fs.createWriteStream('assetImport.csv'));
    result.on('finish', function (err,data) {
      var path = require("path");
      var Converter = require("csvtojson").Converter;
      var converter = new Converter({});
      converter.on("end_parsed", function (jsonArray) {

        if(jsonArray!=null && jsonArray.length>0){
          jsonArray.forEach(function(i){
            AccountDetails.find({"where":{"accountNumber":i.accountNumber}},function(err,response){
              AccountList=response;
              if(response.length>0){
                //console.log("exist "+JSON.stringify(response));
              }else if (employeeDetails.accountNumber==undefined || employeeDetails.accountNumber==null || employeeDetails.accountNumber=='') {
                //console.log("account detail not created bcz its empty")
              }else{
                AccountDetails.create({"accountNumber":i.accountNumber.toString(),"status":"Active"})
              }
            })
          })
          jsonArray.forEach(function(i){
            AssetTypeDetails.find({"where":{"name":i.assetName}},function(err,response){
              AssetTypeList=response;
              if(response.length>0){
                console.log("assetName not created bcz exists..!");
              }else{
                AssetTypeDetails.create({"name":i.assetName.toString(),"status":"Active"})
                }
            })
          })

          jsonArray.forEach(function(i){
            TypeOfDuties.find({"where":{"name":i.dutyType.toLowerCase()}},function(err,response){
              console.log('dutyType----list'+JSON.stringify(response));
              TypeOfDutiesList=response;
              console.log(i.dutyType+'.......duty type....'+i.duty);
              if(response.length>0){
                console.log("dutyType not created bcz exists..!");
              }else{
                if(i.duty==undefined || i.duty=='' || i.duty==null){
                  var message='Please Enter Duty';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                  console.log('duty error ...'+failureStatus);
                }else{
                  TypeOfDuties.create({"name":i.dutyType.toString(),"duty":i.duty.toString(),"status":"Active"})
                }

               /*if(i.duty!=undefined || i.duty!='' || i.duty!=null){
                TypeOfDuties.create({"name":i.dutyType.toString(),"duty":i.duty.toString(),"status":"Active"})
                }else{
                  var message='Please Enter Duty';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                 console.log('duty error ...'+failureStatus);
                }*/
              }
            })
          })

          jsonArray.forEach(function(i){
            DepartmentDetails.find({"where":{"name":i.name}},function(err,response){
              DepartmentList=response;
              if(response.length>0){
                console.log("exist "+JSON.stringify(response));
              }else if (employeeDetails.department==undefined || employeeDetails.department==null || employeeDetails.department=='') {
                console.log("department details not created bcz its empty")
              }else{
                DepartmentDetails.create({"department":i.department.toString(),"status":"Active"})
              }
            })
          })

          jsonArray.forEach(function(i){
            AssetStatusDetails.find({"where":{"name":i.status}},function(err,response){
              AssetStatusList=response;
              if(response.length>0){
                console.log("AssetStatusDetails exist "+JSON.stringify(response));
              }else if (employeeDetails.status==undefined || employeeDetails.status==null || employeeDetails.status=='') {
                console.log("AssetStatusDetails details not created bcz its empty")
              }else{
                AssetStatusDetails.create({"name":i.status.toString(),"status":"Active"})
              }
            })
          })

          jsonArray.forEach(function(i){
            CostCenterDetails.find({"where":{"name":i.costCenter}},function(err,response){
              CostCenterList=response;
              if(response.length>0){
                console.log("CostCenterDetails exist "+JSON.stringify(response));
              }else if (employeeDetails.costCenter==undefined || employeeDetails.costCenter==null || employeeDetails.costCenter=='') {
                console.log("CostCenterDetails details not created bcz its empty")
              }else{
                CostCenterDetails.create({"name":i.costCenter.toString(),"status":"Active"})
              }
            })
          })

          jsonArray.forEach(function(i){
            AssetSparesDetails.find({"where":{"name":i.assetSparesName}},function(err,response){
              AssetSparesList=response;
              if(response.length>0){
                console.log("AssetSparesDetails exist "+JSON.stringify(response));
              }else if (employeeDetails.assetSparesName==undefined || employeeDetails.assetSparesName==null || employeeDetails.assetSparesName=='') {
                console.log("AssetSparesDetails details not created bcz its empty")
              }else{
                AssetSparesDetails.create({"name":i.assetSparesName.toString(),"status":"Active"})
              }
            })
          })
        }
        if(jsonArray!=null && jsonArray.length>0){
           jsonArray.forEach(function(i){
             DepartmentDetails.find({"where":{"name":i.department}},function(err,response){
             DepartmentList=response;
               if(DepartmentList.length>0){
                 console.log("department details not created bcz already exists");
               }else{
                 DepartmentDetails.create({"name": i.department.toString(), "status": "Active"});
               }
             })
           })
         }
        var failureDetails=[];
        //var successfulDetails=[];
        var countDetails=0;
        for(var i=0;i<jsonArray.length;i++){
          var employeeDetails=jsonArray[i];
          if(employeeDetails.accountNumber!=''){
            console.log("602:"+employeeDetails.accountNumber);
            for(var j=0;j<AccountList.length;j++){
              var accNum=AccountList[j];
              console.log("605:"+employeeDetails.accountNumber);
              if(employeeDetails.accountNumber==accNum.accountNumber){
                console.log("607:"+accNum.accountNumber);
                accountNumber.push(accNum.id.toString());
              }else{
                var message='Account Number Miss Match';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
              }
            }
          }

          if(employeeDetails.assetName!=''){
            console.log("602:"+employeeDetails.assetName);
            for(var j=0;j<AssetTypeList.length;j++){
              var assetName=AssetTypeList[j];
              console.log("605:"+employeeDetails.assetName);
              if(employeeDetails.assetName==assetName.assetName){
                console.log("607:"+assetName.assetName);
                assetName.push(assetName.id.toString());
              }else{
                var message='Asset Type Miss Match';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
              }
            }
          }

          if(employeeDetails.department!=''){
            console.log("602:"+employeeDetails.department);
            for(var j=0;j<DepartmentList.length;j++){
              var deptName=DepartmentList[j];
              console.log("605:"+employeeDetails.department);
              if(employeeDetails.department==deptName.department){
                console.log("607:"+deptName.department);
                department.push(deptName.id.toString());
              }else{
                var message='Department Name Miss Match';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
              }
            }
          }
          if(employeeDetails!=undefined && employeeDetails!=null && employeeDetails!=''){
            console.log("emp details are: " +JSON.stringify(employeeDetails))
            var failureStatus=false;
            if(employeeDetails.assetName==undefined || employeeDetails.assetName==null || employeeDetails.assetName==''){
              var message='Please Enter Asset Name';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            if(employeeDetails.assetDescription==undefined || employeeDetails.assetDescription==null || employeeDetails.assetDescription==''){
              var message='Please Enter Asset Description';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            if(employeeDetails.assetType==undefined || employeeDetails.assetType==null || employeeDetails.assetType==''){
              var message='Please Enter Valid Asset Type';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            if(employeeDetails.locality==undefined || employeeDetails.locality==null || employeeDetails.locality==''){
              var message='Please Enter Location';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            if(employeeDetails.assetRenewableAvailability!=undefined && employeeDetails.assetRenewableAvailability!="" && employeeDetails.assetRenewableAvailability!=null){
              if(employeeDetails.assetRenewableAvailability.toLowerCase()== "yes"){
                if(employeeDetails.assetUpgradeDate!=undefined && employeeDetails.assetUpgradeDate!="" && employeeDetails.assetUpgradeDate!=null){
                }
                else{
                  var message='Please Select Asset Upgrade Date';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                  failureDetails.push(employeeDetails);
                }
              }else{
                delete employeeDetails.assetUpgradeDate;
                //console.log("*********delete key ************" +JSON.stringify(employeeDetails))
              }
            }
            if(employeeDetails.accountNumber==undefined || employeeDetails.accountNumber==null || employeeDetails.accountNumber==''){
              var message='Please Enter Account Number';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            /*if(employeeDetails.department==undefined || employeeDetails.department==null || employeeDetails.department==''){
              var message='Please Enter Department';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }*/
            if(employeeDetails.assetMode==undefined || employeeDetails.assetMode==null || employeeDetails.assetMode==''){
              var message='Please Enter Asset is owned/managed';
              employeeDetails['errorMessage']=message;
              failureStatus=true;
              failureDetails.push(employeeDetails);
            }
            if(employeeDetails.assetMode!=undefined && employeeDetails.assetMode!="" && employeeDetails.assetMode!=null) {
              if(employeeDetails.assetMode=="owned"){
                if(employeeDetails.transferDate!=undefined && employeeDetails.transferDate!="" && employeeDetails.transferDate!=null){
                  if(employeeDetails.department!=undefined && employeeDetails.department!="" && employeeDetails.department!=null){

                    if(employeeDetails.regdNo!=undefined && employeeDetails.regdNo!="" && employeeDetails.regdNo!=null){

                    }
                    else{
                      var message='Please Enter Regd Number';
                      employeeDetails['errorMessage']=message;
                      failureStatus=true;
                      failureDetails.push(employeeDetails);
                    }
                  }
                  else{
                    var message='Please Enter Department';
                    employeeDetails['errorMessage']=message;
                    failureStatus=true;
                    failureDetails.push(employeeDetails);
                  }
                }
                else{
                  var message='Please Enter Transfer Date';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                  failureDetails.push(employeeDetails);
                }
              }else{
                delete employeeDetails.transferDate;
                delete employeeDetails.department;
                delete employeeDetails.regdNo;
                //console.log("*********delete key ************" +JSON.stringify(employeeDetails))
              }
              if(employeeDetails.assetMode=="managed"){
                if(employeeDetails.ownerName!=undefined && employeeDetails.ownerName!="" && employeeDetails.ownerName!=null){

                  if(employeeDetails.phoneNumber!=undefined && employeeDetails.phoneNumber!="" && employeeDetails.phoneNumber!=null){

                    if(employeeDetails.addressDetails!=undefined && employeeDetails.addressDetails!="" && employeeDetails.addressDetails!=null){

                      if(employeeDetails.leasedDate!=undefined && employeeDetails.leasedDate!="" && employeeDetails.leasedDate!=null){

                        if(employeeDetails.termsInMonths!=undefined && employeeDetails.termsInMonths!="" && employeeDetails.termsInMonths!=null){

                        }
                        else{
                          var message='Please Enter Terms In Months';
                          employeeDetails['errorMessage']=message;
                          failureStatus=true;
                          failureDetails.push(employeeDetails);
                        }
                      }
                      else{
                        var message='Please Enter Leased Date';
                        employeeDetails['errorMessage']=message;
                        failureStatus=true;
                        failureDetails.push(employeeDetails);
                      }
                    }
                    else{
                      var message='Please Enter Address Details';
                      employeeDetails['errorMessage']=message;
                      failureStatus=true;
                      failureDetails.push(employeeDetails);
                    }
                  }
                  else{
                    var message='Please Enter Valid Mobile Number';
                    employeeDetails['errorMessage']=message;
                    failureStatus=true;
                    failureDetails.push(employeeDetails);
                  }
                }
                else{
                  var message='Please Enter Owner Name';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                  failureDetails.push(employeeDetails);
                }
              }else{
                delete employeeDetails.ownerName;
                delete employeeDetails.phoneNumber;
                delete employeeDetails.addressDetails;
                delete employeeDetails.leasedDate;
                delete employeeDetails.termsInMonths;
              }
            }

            if(employeeDetails.historicCostAvailability!=undefined && employeeDetails.historicCostAvailability!="" && employeeDetails.historicCostAvailability!=null){
              if(employeeDetails.historicCostAvailability.toLowerCase()== "yes"){
                if(employeeDetails.historicCost==undefined || employeeDetails.historicCost=="" || employeeDetails.historicCost==null){
                  var message='Please Enter Historic Cost';
                  employeeDetails['errorMessage']=message;
                  failureStatus=true;
                  failureDetails.push(employeeDetails);
                }else if(employeeDetails.historicCostAvailability.toLowerCase()=='no'){
                  if(employeeDetails.initialValuationCost==undefined || employeeDetails.initialValuationCost=='' || employeeDetails.initialValuationCost==null){
                    var message='Please Enter Initial Valuation Cost';
                    employeeDetails['errorMessage']=message;
                    failureStatus=true;
                    failureDetails.push(employeeDetails);
                  }
                }
              }else{
                delete employeeDetails.historicCost;
                delete employeeDetails.initialValuationCost;
                //console.log("*********delete key ************" +JSON.stringify(employeeDetails))
              }

              if(employeeDetails.contactPersonDepartment==undefined || employeeDetails.contactPersonDepartment==null || employeeDetails.contactPersonDepartment==''){
                var message='Please Enter Contact Person Department';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.contactPerson==undefined || employeeDetails.contactPerson==null || employeeDetails.contactPerson==''){
                var message='Please Enter Contact Person';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }


              if(employeeDetails.landDetails==undefined || employeeDetails.landDetails==null || employeeDetails.landDetails==''){
                var message='Please Enter Land Details';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.areaInUnits==undefined || employeeDetails.areaInUnits==null || employeeDetails.areaInUnits==''){
                var message='Please Enter Area In Units';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.village==undefined || employeeDetails.village==null || employeeDetails.village==''){
                var message='Please Enter Village';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.longitude==undefined || employeeDetails.longitude==null || employeeDetails.longitude==''){
                var message='Please Enter longitude';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.latitude==undefined || employeeDetails.latitude==null || employeeDetails.latitude==''){
                var message='Please Enter latitude';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.ward==undefined || employeeDetails.ward==null || employeeDetails.ward==''){
                var message='Please Enter ward';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.muncipalityOrMandal==undefined || employeeDetails.muncipalityOrMandal==null || employeeDetails.muncipalityOrMandal==''){
                var message='Please Enter Muncipality Or Mandal';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.registrationDetails==undefined || employeeDetails.registrationDetails==null || employeeDetails.registrationDetails==''){
                var message='Please Enter Registration Details';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.status==undefined || employeeDetails.status==null || employeeDetails.status==''){
                var message='Please Enter status';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.commissionDate==undefined || employeeDetails.commissionDate==null || employeeDetails.commissionDate==''){
                var message='Please Enter commissionDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.commissionDate==undefined || employeeDetails.commissionDate==null || employeeDetails.commissionDate==''){
                var message='Please Enter commissionDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }


              if(employeeDetails.decommissionDate==undefined || employeeDetails.decommissionDate==null || employeeDetails.decommissionDate==''){
                var message='Please Enter De-CommissionDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.dutyType==undefined || employeeDetails.dutyType==null || employeeDetails.dutyType==''){
                var message='Please Enter Duty Type';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.duty==undefined || employeeDetails.duty==null || employeeDetails.duty==''){
                var message='Please Enter Duty';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.maintenanceContactDepartment==undefined || employeeDetails.maintenanceContactDepartment==null || employeeDetails.maintenanceContactDepartment==''){
                var message='Please Enter Maintenance Contact Department';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.maintenanceContactName==undefined || employeeDetails.maintenanceContactName==null || employeeDetails.maintenanceContactName==''){
                var message='Please Enter Maintenance Contact Name';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.originalLife==undefined || employeeDetails.originalLife==null || employeeDetails.originalLife==''){
                var message='Please Enter Original Life';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.expectedDateOfDisposal==undefined || employeeDetails.expectedDateOfDisposal==null || employeeDetails.expectedDateOfDisposal==''){
                var message='Please Enter Expected Date Of Disposal';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.originalRemainingLife==undefined || employeeDetails.originalRemainingLife==null || employeeDetails.originalRemainingLife==''){
                var message='Please Enter Original Remaining Life';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.revisedRemainingLife==undefined || employeeDetails.revisedRemainingLife==null || employeeDetails.revisedRemainingLife==''){
                var message='Please Enter revisedRemainingLife';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.replacementDueDate==undefined || employeeDetails.replacementDueDate==null || employeeDetails.replacementDueDate==''){
                var message='Please Enter replacementDueDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.assetLastReviewedDate==undefined || employeeDetails.assetLastReviewedDate==null || employeeDetails.assetLastReviewedDate==''){
                var message='Please Enter assetLastReviewedDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.costCenter==undefined || employeeDetails.costCenter==null || employeeDetails.costCenter==''){
                var message='Please Enter costCenter';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.amortizationMethod==undefined || employeeDetails.amortizationMethod==null || employeeDetails.amortizationMethod==''){
                var message='Please Enter amortizationMethod';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.amortizationMethod==undefined || employeeDetails.amortizationMethod==null || employeeDetails.amortizationMethod==''){
                var message='Please Enter amortizationMethod';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.amortizationRate==undefined || employeeDetails.amortizationRate==null || employeeDetails.amortizationRate==''){
                var message='Please Enter amortizationRate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.amortizationAmount==undefined || employeeDetails.amortizationAmount==null || employeeDetails.amortizationAmount==''){
                var message='Please Enter amortizationAmount';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.bookValue==undefined || employeeDetails.bookValue==null || employeeDetails.bookValue==''){
                var message='Please Enter bookValue';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.returnValue==undefined || employeeDetails.returnValue==null || employeeDetails.returnValue==''){
                var message='Please Enter returnValue';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }

              if(employeeDetails.scrapValue==undefined || employeeDetails.scrapValue==null || employeeDetails.scrapValue==''){
                var message='Please Enter scrapValue';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.controlAccount==undefined || employeeDetails.controlAccount==null || employeeDetails.controlAccount==''){
                var message='Please Enter controlAccount';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.adjustmentAccount==undefined || employeeDetails.adjustmentAccount==null || employeeDetails.adjustmentAccount==''){
                var message='Please Enter adjustmentAccount';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.shrinkageAccount==undefined || employeeDetails.shrinkageAccount==null || employeeDetails.shrinkageAccount==''){
                var message='Please Enter shrinkageAccount';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.priceVariance==undefined || employeeDetails.priceVariance==null || employeeDetails.priceVariance==''){
                var message='Please Enter priceVariance';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
               if(employeeDetails.currencyVariance==undefined || employeeDetails.currencyVariance==null || employeeDetails.currencyVariance==''){
                var message='Please Enter currencyVariance';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
               if(employeeDetails.purchasePriceVariance==undefined || employeeDetails.purchasePriceVariance==null || employeeDetails.purchasePriceVariance==''){
                var message='Please Enter purchasePriceVariance';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
               if(employeeDetails.receiptVariance==undefined || employeeDetails.receiptVariance==null || employeeDetails.receiptVariance==''){
                var message='Please Enter receiptVariance';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
               if(employeeDetails.criticalityAsset==undefined || employeeDetails.criticalityAsset==null || employeeDetails.criticalityAsset==''){
                var message='Please Enter criticalityAsset';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.disposalMethod==undefined || employeeDetails.disposalMethod==null || employeeDetails.disposalMethod==''){
                var message='Please Enter disposalMethod';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.deCommissionStatus==undefined || employeeDetails.deCommissionStatus==null || employeeDetails.deCommissionStatus==''){
                var message='Please Enter deCommissionStatus';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.sinceDate==undefined || employeeDetails.sinceDate==null || employeeDetails.sinceDate==''){
                var message='Please Enter sinceDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.acquisitionStatus==undefined || employeeDetails.acquisitionStatus==null || employeeDetails.acquisitionStatus==''){
                var message='Please Enter acquisitionStatus';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.ownerShipTransferStatus==undefined || employeeDetails.ownerShipTransferStatus==null || employeeDetails.ownerShipTransferStatus==''){
                var message='Please Enter ownerShipTransferStatus';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.requestTransferName==undefined || employeeDetails.requestTransferName==null || employeeDetails.requestTransferName==''){
                var message='Please Enter requestTransferName';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.requiredDate==undefined || employeeDetails.requiredDate==null || employeeDetails.requiredDate==''){
                var message='Please Enter requiredDate';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.legalFormalityStatus==undefined || employeeDetails.legalFormalityStatus==null || employeeDetails.legalFormalityStatus==''){
                var message='Please Enter legalFormalityStatus';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.assetSparesName==undefined || employeeDetails.assetSparesName==null || employeeDetails.assetSparesName==''){
                var message='Please Enter assetSparesName';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.assetSparesComment==undefined || employeeDetails.assetSparesComment==null || employeeDetails.assetSparesComment==''){
                var message='Please Enter assetSparesComment';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.acquisitionComments==undefined || employeeDetails.acquisitionComments==null || employeeDetails.acquisitionComments==''){
                var message='Please Enter acquisitionComments';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }
              if(employeeDetails.impairmentLife==undefined || employeeDetails.impairmentLife==null || employeeDetails.impairmentLife==''){
                var message='Please Enter impairmentLife';
                employeeDetails['errorMessage']=message;
                failureStatus=true;
                failureDetails.push(employeeDetails);
              }


            }

            if(!failureStatus){
              var AssetForLand=server.models.AssetForLand;
              var employeeInfo=employeeDetails;
              console.log("******employee information******" +JSON.stringify(employeeInfo));
              var employeeObject={
                "assetName": employeeInfo.assetName.toString(),
                "assetDescription": employeeInfo.assetDescription,
                "assetType": employeeInfo.assetType,
                "locality": employeeInfo.locality,
                "assetRenewableAvailability": employeeInfo.assetRenewableAvailability,
                "assetUpgradeDate": employeeInfo.assetUpgradeDate,
                "accountNumber": employeeInfo.accountNumber.toString(),
                "assetMode": employeeInfo.assetMode,
                "ownerName": employeeInfo.ownerName,
                "phoneNumber": employeeInfo.phoneNumber,
                "addressDetails": employeeInfo.addressDetails,
                "leasedDate": employeeInfo.leasedDate,
                "termsInMonths": employeeInfo.termsInMonths,
                "transferDate": employeeInfo.transferDate,
                "department": employeeInfo.department,
                "regdNo": employeeInfo.regdNo,
                "contactPersonDepartment":employeeInfo.contactPersonDepartment,
                "contactPerson":employeeInfo.contactPerson,
                "landDetails":employeeInfo.landDetails,
                "areaInUnits":employeeInfo.areaInUnits,
                "village":employeeInfo.village,
                "longitude":employeeInfo.longitude,
                "latitude":employeeInfo.latitude,
                "ward":employeeInfo.ward,
                "muncipalityOrMandal":employeeInfo.muncipalityOrMandal,
                "registrationDetails":employeeInfo.registrationDetails,
                "status":employeeInfo.status,
                "commissionDate":employeeInfo.commissionDate,
                "decommissionDate":employeeInfo.decommissionDate,
                "dutyType":employeeInfo.dutyType,
                "duty":employeeInfo.duty,
                "maintenanceContactDepartment":employeeInfo.maintenanceContactDepartment,
                "maintenanceContactName":employeeInfo.maintenanceContactName,
                "originalLife":employeeInfo.originalLife,
                "expectedDateOfDisposal":employeeInfo.expectedDateOfDisposal,
                "originalRemainingLife":employeeInfo.originalRemainingLife,
                "revisedRemainingLife":employeeInfo.revisedRemainingLife,
                "replacementDueDate":employeeInfo.replacementDueDate,
                "assetLastReviewedDate":employeeInfo.assetLastReviewedDate,
                "costCenter":employeeInfo.costCenter,
                "historicCostAvailability":employeeInfo.historicCostAvailability,
                "historicCost":employeeInfo.historicCost,
                "initialValuationCost":employeeInfo.initialValuationCost,
                "amortizationMethod":employeeInfo.amortizationMethod,
                "amortizationRate":employeeInfo.amortizationRate,
                "amortizationAmount":employeeInfo.amortizationAmount,
                "bookValue":employeeInfo.bookValue,
                "returnValue":employeeInfo.returnValue,
                "scrapValue":employeeInfo.scrapValue,
                "controlAccount":employeeInfo.controlAccount,
                "adjustmentAccount":employeeInfo.adjustmentAccount,
                "shrinkageAccount":employeeInfo.shrinkageAccount,
                "priceVariance":employeeInfo.priceVariance,
                "currencyVariance":employeeInfo.currencyVariance,
                "purchasePriceVariance":employeeInfo.purchasePriceVariance,
                "receiptVariance":employeeInfo.receiptVariance,
                "criticalityAsset":employeeInfo.criticalityAsset,
                "disposalMethod":employeeInfo.disposalMethod,
                "deCommissionStatus":employeeInfo.deCommissionStatus,
                "sinceDate":employeeInfo.sinceDate,
                "acquisitionStatus":employeeInfo.acquisitionStatus,
                "ownerShipTransferStatus":employeeInfo.ownerShipTransferStatus,
                "requestTransferName":employeeInfo.requestTransferName,
                "requiredDate":employeeInfo.requiredDate,
                "legalFormalityStatus":employeeInfo.legalFormalityStatus,
                "assetSparesName":employeeInfo.assetSparesName,
                "assetSparesComment":employeeInfo.assetSparesComment,
                "acquisitionComments":employeeInfo.acquisitionComments,
                "impairmentLife":employeeInfo.impairmentLife
              };
              console.log("employee object" +JSON.stringify(employeeObject));
              AssetForLand.create(employeeObject,function(err, createdData){
                if(err){
                  var message='This Asset Name is already existed';
                  employeeInfo['errorMessage']=message;
                  failureStatus=true;
                  failureDetails.push(employeeInfo);
                  countDetails++;
                  if(countDetails==jsonArray.length){
                    cb(null, failureDetails)
                  }
                }else{
                  employeeInfo['errorMessage']='success';
                  failureDetails.push(employeeInfo);
                  countDetails++;
                  if(countDetails==jsonArray.length){
                    cb(null, failureDetails)
                  }
                }
              });
            }else{
              countDetails++;
              console.log("count details" +countDetails);
              console.log("json array" +JSON.stringify(failureDetails));
              if(countDetails==jsonArray.length){
                cb(null, failureDetails)
              }
            }
          }else{
            countDetails++;
            if(countDetails==jsonArray.length){
              cb(null, failureDetails)
            }
          }
        }
      });
      require("fs").createReadStream(path.join('assetImport.csv')).pipe(converter);
    });
  };

    AssetForLand.observe('loaded', function(ctx, next) {

     if(ctx.instance) {
          if(ctx.instance.department!=undefined && ctx.instance.department!=null && ctx.instance.department!='' ){
          var department = server.models.AssetDepartments;
          var deptId=ctx.instance.department;
          var contactPersonDepartmentId=ctx.instance.contactPersonDepartment;
          var maintenanceContactDepartmentId=ctx.instance.maintenanceContactDepartment;
          department.findById(deptId,function(err,dept){
          if(dept!=undefined && dept){
          var departmentValue=dept.name;
                 ctx.instance['departmentValue'] = departmentValue;
                  department.findById(contactPersonDepartmentId,function(err,contactPersonDept)
                          {
                             if(contactPersonDept!=undefined && contactPersonDept)
                             {
                               var contactDeptValue=contactPersonDept.name;
                               ctx.instance['contactDeptValue']=contactDeptValue;
                             }
                              department.findById(maintenanceContactDepartmentId,function(err,maintenanceContactDept)
                                         {
                                            if(maintenanceContactDept!=undefined && maintenanceContactDept)
                                            {
                                              var maintenanceContactDepartmentValue=maintenanceContactDept.name;
                                              ctx.instance['maintenanceContactDepartmentValue']=maintenanceContactDepartmentValue;
                                            }
                                             next();
                                         });
                          });
          }else{
          next();
          }
        });
          }else{
          next();
          }
        }
            else{
              next();
            }
          });

  //to get data for the bar chart to show the department wise assets
     AssetForLand.getDeptWiseAssetReport = function (cb) {
      var assetDepartmentsList = server.models.AssetDepartments;
      var finalResult=[],O=[],M=[],OT=[];
      assetDepartmentsList.find({"where":{"status":"Active"}}, function(err, assetDepartments){
        var i=0,x;
        assetDepartments.forEach(function(assetDept){
          owned=0,managed=0,others=0;
          var temp= assetDept;

          AssetForLand.find({"where":{"departmentValue": assetDept.name}}, temp, function(err, assetsUnderDept){
            i++;
            if(assetsUnderDept.length>0){
              owned  = assetsUnderDept.reduce(function(total,x){return x.assetMode.toLowerCase() == 'owned' ? total+1 : total}, 0);
              managed  = assetsUnderDept.reduce(function(total,x){return x.assetMode.toLowerCase() == 'managed' ? total+1 : total}, 0);
              others  = assetsUnderDept.reduce(function(total,x){return x.assetMode.toLowerCase() == 'others' ? total+1 : total}, 0);
              O.push(owned);
              M.push(managed);
              OT.push(others);
            }else{
              O.push(0);
              M.push(0);
              OT.push(0);
            }
            finalResult=[
              {
                "name": "Owned",
                "data": O
              },
              {
                "name": "Managed",
                "data": M
              },
              {
                "name": "Others",
                "data": OT
              }
            ];
            if(i == assetDepartments.length){
              cb(null, finalResult);
            }
          }.bind(temp));

        });
      });
    };

     AssetForLand.remoteMethod('getDeptWiseAssetReport', {
      description: "Send Valid EmployeeId",
      returns: {
        arg: 'data',
        type: "object",
        root:true
      },
      http: {
        path: '/getDeptWiseAssetReport',
        verb: 'GET'
      }
    });

    // Remote method for getting comission and de-commision date based reports
      AssetForLand.getReportsOnComAndDeCom = function (cb) {
     var todayDate=new Date();
     var year=todayDate.getFullYear();
      var firstYearCommCount=0;
      var secondYearCommCount=0;
      var thirdYearCommCount=0;
      var fourthYearCommCount=0;
      var fiveYearCommCount=0;

      var firstYearDeCommCount=0;
      var secondYearDeCommCount=0;
      var thirdYearDeCommCount=0;
      var fourthYearDeCommCount=0;
      var fiveYearDeCommCount=0;

       AssetForLand.find({},function(err, commsionList){
           if(commsionList && commsionList.length>0 ){
              for(var i=0;i<commsionList.length;i++){
              if(commsionList[i].status && commsionList[i].status!='' && commsionList[i].status=="commissioned"){
                if(commsionList[i].commissionDate && commsionList[i].commissionDate !=null && commsionList[i].commissionDate!=''){
                 console.log("commission date" +commsionList[i].commissionDate)
                                  var yearValue=commsionList[i].commissionDate.split('-');
                                   console.log("commision year" +yearValue)
                                    if(yearValue && yearValue.length>0 && yearValue.length==3){
                                        if(parseInt(yearValue[2]) ==year){
                                                firstYearCommCount++;
                                                //firstYearComm.push(commsionList[i]);
                                        }else if(parseInt(yearValue[2]) ==(year+1)){
                                                secondYearCommCount++;
                                        }else if(parseInt(yearValue[2]) ==(year+2)){
                                                thirdYearCommCount++;
                                        }else if(parseInt(yearValue[2]) ==(year+3)){
                                                fourthYearCommCount++;
                                         }else if(parseInt(yearValue[2]) ==(year+4)){
                                                fiveYearCommCount++;
                                          }
                                    }
                        }

            } else if(commsionList[i].status && commsionList[i].status!='' && (commsionList[i].status=="decommissioned"||commsionList[i].status=="disposal" )){
                                 if(commsionList[i].decommissionDate && commsionList[i].decommissionDate !=null && commsionList[i].decommissionDate!=''){

                                 console.log("decommission date" +commsionList[i].decommissionDate)


                                                   var yearValue=commsionList[i].decommissionDate.split('-');

                                                   console.log("year" +yearValue)

                                                     if(yearValue && yearValue.length>0 && yearValue.length==3 ){
                                                         if(parseInt(yearValue[2]) ==year){
                                                         firstYearDeCommCount++;
                   //                                        firstYearComm.push(commsionList[i]);
                                                         }else if(parseInt(yearValue[2]) ==(year+1)){
                                                         secondYearDeCommCount++;
                                                         }else if(parseInt(yearValue[2]) ==(year+2)){
                                                                thirdYearDeCommCount++;
                                                         }else if(parseInt(yearValue[2]) ==(year+3)){
                                                          fourthYearDeCommCount++;

                                                          }else if(parseInt(yearValue[2]) ==(year+4)){
                                                          fiveYearDeCommCount++;

                                                     }
                                             }
                               }
                             }

                       }


                      var finalObject=[
                      {
                       name: 'Commissioned',
                       data: [firstYearCommCount, secondYearCommCount, thirdYearCommCount, fourthYearCommCount, fiveYearCommCount]
                      },
                      {
                       name: 'Decommissioned',
                       data: [firstYearDeCommCount, secondYearDeCommCount, thirdYearDeCommCount, fourthYearDeCommCount, fiveYearDeCommCount]
                      }
                      ];

                       /*var finalObject = [{
                                             year:year,
                                             data:[firstYearCommCount,firstYearDeCommCount]
                                             },{
                                             year:year + 1,
                                             data:[secondYearCommCount,secondYearDeCommCount]
                                             },{
                                             year:year + 2,
                                             data:[thirdYearCommCount,thirdYearDeCommCount]
                                             },{
                                             year:year + 3,
                                             data:[fourthYearCommCount,fourthYearDeCommCount]
                                             },{
                                             year:year + 4,
                                             data:[fiveYearCommCount,fiveYearDeCommCount]
                                             }]*/
                       cb(null, finalObject);

           }else{
              cb(null,[]);
           }
       })

      };

      AssetForLand.remoteMethod('getReportsOnComAndDeCom', {
      description: "Please enter valid details",
      returns: {
        arg: 'data',
        type: "array",
        root:true
      },
      http: {
        path: '/getReportsOnComAndDeCom',
        verb: 'GET'
      }
    });

    // Remote method for getting expiry reports

      AssetForLand.getExpiredReports = function (data,cb) {

       var todayDate=new Date();
       var year = todayDate.getFullYear();
       var month = todayDate.getMonth();
       var day = todayDate.getDate();
           if(data.type=='1'){
                      startDate=new Date(year,month+3,day);
    //                startDate=new Date(new Date() + (90 * 24 * 60 * 60 * 1000));
                  } else  if(data.type=='2'){
                  startDate=new Date(year,month+6,day);
    //                startDate=new Date(new Date() + (180 * 24 * 60 * 60 * 1000));
                  }else{
                    startDate=new Date(year+1,month,day);
                  }

                  AssetForLand.find({"where" : {"assetType" : "Movable"}},function(err, assetList){
                  var finalObject=[];
                  if(assetList!=undefined && assetList.length>0){

                            var count=0;
                            for (var i=0;i < assetList.length;i++) {

                            if(assetList[i].expiryDate && assetList[i].expiryDate !=null && assetList[i].expiryDate!=''){
                              var expiryDate = assetList[i].expiryDate.split("-");
                              var expiryDateAndTime = new Date(expiryDate[2], expiryDate[1] - 1, expiryDate[0]);
                              console.log('experied date '+expiryDateAndTime);
                              console.log('today date'+todayDate);
                              console.log('satart date '+startDate);
                              if(expiryDateAndTime.getTime()>todayDate.getTime() && expiryDateAndTime.getTime()<startDate.getTime()){
                              finalObject.push(assetList[i]);

                              }
                              }
                            }
                             cb(null,finalObject);
                          }else{
                          cb(null,[])
                          }





                  })
       };

      AssetForLand.remoteMethod('getExpiredReports', {
        description: "Please enter valid details",
        returns: {
          arg: 'data',
          type: "array",
          root:true
        },
         accepts: [{arg: 'filter', type: 'object', http: {source: 'body'}}],
        http: {
          path: '/getExpiredReports',
          verb: 'POST'
        }
      });

      AssetForLand.remoteMethod('getVehicleExpenses', {
          description: "Send Valid Details",
          returns: {
            arg: 'data',
            type: "object",
            root:true
          },
          http: {
            path: '/getVehicleExpenses',
            verb: 'GET'
          }
        });

      AssetForLand.getVehicleExpenses = function (cb) {
          var submitBills = server.models.SubmitBills;
          var i=0;
          var temp='', total =0;
          var finalResult = [];
          submitBills.find({"where":{"finalStatus" : "Approval"}},temp, function(err, assetBills){
          AssetForLand.find({},function(err, assetList){
          assetList.forEach(function(asset){
              temp = asset;
              if(assetList.length>0){
                for(var x=0;x<assetBills.length;x++){
                  //console.log("response+++++++++" +JSON.stringify(asset.assetNumber));
                  if(asset.assetNumber == assetBills[x].selectAsset){
                       total = total + Number(assetBills[x].amount);
                    }
                }
                //console.log("--------------------",finalResult)
                 if(i == assetBills.length){
                     cb(null, finalResult);
                 }
                 finalResult.push({"assetNumber":asset.assetNumber,"total":total});
              }else{

              }

          });
            cb(null, finalResult);
            }.bind(temp));


          });
          //cb(null, finalResult)

        };
};





