var server = require('../../server/server');
module.exports = function(Status) {
  Status.validatesUniquenessOf('field1', {message: 'Status could be unique'});

  Status.observe('before save', function (ctx, next) {

    if (ctx.instance != undefined && ctx.instance != null) {
      ctx.instance.field1=(ctx.instance.field1.toLowerCase());
      ctx.instance.createdTime = new Date();
      next();

    } else {
      if(ctx.data.field1!=undefined && ctx.data.field1!=null && ctx.data.field1!=''){
        ctx.data.field1=(ctx.data.field1.toLowerCase());
      }

      ctx.data.updatedTime = new Date();
      next();



    }
  });
};
