var server = require('../../server/server');
module.exports = function(Workflowemployees) {
  Workflowemployees.observe('before save', function (ctx, next) {


    if(ctx.instance){
      ctx.instance.createdTime=new Date();
      next();

    }else{
      ctx.data.updateTime=new Date();
      next();
    }
  });

};
