var server = require('../../server/server');

module.exports = function(Departmentwisebudget) {
  Departmentwisebudget.observe('before save', function (ctx, next) {

    if(ctx.instance){
      ctx['createdTime']=new Date();
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "departmentWise"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                }
              }
              ctx.instance.workflow = workflowData;
              ctx.instance.workflowId=workflowForm.workflowId;
              ctx.instance.maxlevel=flowDataList.maxLevel;
              ctx.instance.acceptLevel=0;
              ctx.instance.finalStatus=false;
              ctx.instance.createdTime=new Date();
              next();
            });

          });
        }else{
          next('Please Assign Workflow for Department wise Budget', null);
        }
      });
    }else{
      ctx['updatedTime']=new Date();
      next();
    }
  });

  Departmentwisebudget.observe('loaded', function (ctx, next) {
    var Department=server.models.PlanDepartment;
    var ProjectPlan=server.models.ProjectPlan;
    if(ctx.instance){
      function getWorkflowData() {
        if (ctx.instance.workflowId != undefined && ctx.instance.workflowId != null && ctx.instance.workflowId != '') {
          var WorkflowEmployees = server.models.WorkflowEmployees;
          WorkflowEmployees.find({'where': {'and': [{'workflowId': ctx.instance.workflowId}, {"status": "Active"}]}}, function (err, workflowEmployeeList) {
            if (workflowEmployeeList != null && workflowEmployeeList.length > 0) {
              ctx.instance.workflowData = workflowEmployeeList;
              console.log(JSON.stringify(ctx.instance.planDetails));
              if(ctx.instance.planDetails!=undefined && ctx.instance.planDetails!=null && ctx.instance.planDetails.length>0){
                var planId=[];
                for(var i=0;i<ctx.instance.planDetails.length;i++){
                  planId.push({'id':ctx.instance.planDetails[i].id});
                }
                if(planId.length>0){
                  ProjectPlan.find({'where':{'or':planId}}, function (err, planData) {
                    ctx.instance['planData']=planData;
                    next();
                  })
                }else{
                  next();
                }
              }else{
                next();
              }
            } else {
              next();
            }
          })
        } else {
          next();
        }
      }

      if(ctx.instance.departmentId){
      if(ctx.instance.departmentId !='all'){
        Department.findById(ctx.instance.departmentId, function(err, deptInfo){
          if(err){
          }else{
            ctx.instance['department'] = deptInfo.name;
          }
          getWorkflowData();
        });
      }else{
        ctx.instance['department'] = 'All';
       getWorkflowData();
      }
      }else{
       getWorkflowData();
      }
    }else{
      next();
    }
  });

  Departmentwisebudget.getDetails = function (employee, cb) {

    Departmentwisebudget.find({},function(err, deptBudgetData){
      var fieldVisitList=[];
      var adminStatus=false;
      if(deptBudgetData!=null && deptBudgetData.length>0){
         var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            } else {
            }
            for (var i = 0; i < deptBudgetData.length; i++) {
              var data = deptBudgetData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }
                    }
                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }
              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });

      }
      else{
        cb(null,fieldVisitList);
      }
    });
  };


  Departmentwisebudget.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });

  Departmentwisebudget.updateDetails = function (approvalDetails, cb) {


    Departmentwisebudget.findOne({'where':{'id':approvalDetails.budgetId}},function(err, budgetInfo){

      if(budgetInfo!=undefined && budgetInfo!=null){
        var finalObject={};
        var workflowDetails=budgetInfo.workflow;
        if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
          var workflowData=[];
          for(var  i=0;i< workflowDetails.length;i++){
            var data=workflowDetails[i];
            var workFlowLevel=parseInt(workflowDetails[i].levelNo);
            if((approvalDetails.acceptLevel+1)==workFlowLevel){
              data.approvalStatus=approvalDetails.approvedStatus;
              data.employeeName=approvalDetails.approvedBy;
              data.employeeId=approvalDetails.employeeId;
              data.comment=approvalDetails.comment;
              data['approvedBudget']=approvalDetails.approvedBudget
              data.date=new Date();
            }
            workflowData.push(data);

          }
          finalObject.workflow=workflowData;
        }
        if(approvalDetails.approvedStatus=="Approved"){
          if(budgetInfo.maxlevel==(approvalDetails.acceptLevel+1)){
            finalObject.finalStatus=approvalDetails.approvedStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          }else{
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          }
        } else if(approvalDetails.approvedStatus=="Rejected"){
          finalObject.finalStatus=approvalDetails.approvedStatus;
          finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          finalObject.rejectComment=approvalDetails.comment;
        }
        finalObject['approvedBudget']=approvalDetails.approvedBudget;
        console.log('before save details'+JSON.stringify(finalObject));
        budgetInfo.updateAttributes(finalObject, function(err, finalData){
          cb(null, finalData);
        });


      }else {
        var message = 'Please provide valid details';
        cb(message,null);
      }

    });


  };

  Departmentwisebudget.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Departmentwisebudget.updateContent= function (updatedDetails, cb) {
    Departmentwisebudget.findOne({'where':{'id':updatedDetails.id}},function(err, budgetData){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "departmentWise"}]}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  };
                  workflowData.push(flowData);
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails['updatedTime']=new Date();
              budgetData.updateAttributes(updatedDetails,function(err,data){
                console.log('successfully updated');
              });
              cb(null, budgetData);
            });

          });
        }
      });
    });
  };

  Departmentwisebudget.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });

  Departmentwisebudget.getBudgetPlans= function (ulb,deptId,fYear, cb) {
    var taskStatus=server.models.TaskStatus;
    var ProjectPlan=server.models.ProjectPlan;
    Departmentwisebudget.find({},function(err, budgetData){
      if(budgetData.length>0){

        taskStatus.find({},function(err,taskData){
          if(taskData.length>0){
            var taskArray=[];
            for(var i=0;i<taskData.length;i++){
              if(taskData[i].name=='new' || taskData[i].name=='pending' || taskData[i].name=='working'){
                taskArray.push({"taskStatusId":taskData[i].id+''});
              }
            }
            if (deptId == 'all') {
              var finalData=[];
              finalData.push({'ulb':ulb});
              finalData.push({'financialYear':fYear});
              finalData.push({'or':taskArray});
              console.log('details before '+JSON.stringify(finalData));
              ProjectPlan.find({'where': {'and': finalData}}, function (err, planData) {
                var finalObject = [];
                if (planData!=null && planData.length > 0) {
                  for(var i=0;i<planData.length;i++){
                    var findStatus=false;
                    if(planData[i].finalStatus!=undefined && planData[i].finalStatus!=null && planData[i].finalStatus== "Approval"){
                      for(j=0;j<budgetData.length;j++){
                        if(budgetData[j].finalStatus!=undefined && budgetData[j].finalStatus!=null && budgetData[j].finalStatus!= "Rejected"){
                          if(budgetData[j].planDetails!=undefined && budgetData[j].planDetails!=null && budgetData[j].planDetails.length>0){
                            var planDetails=budgetData[j].planDetails;
                            for(var x=0;x<planDetails.length;x++){
                              if(planDetails[x].planId==planData[i].planId){
                                findStatus=true;
                                break;
                              }
                            }
                          }
                        }
                        if(findStatus){
                          break;
                        }
                      }
                      if(!findStatus){
                        finalObject.push(planData[i]);
                      }
                    }

                  }
                  cb(null,finalObject);
                } else {
                  cb(null, finalObject);
                }
              })
            }else{
              var finalData=[];
              finalData.push({'ulb':ulb});
              finalData.push({'financialYear':fYear});
              finalData.push({'or':taskArray});
              finalData.push({'departmentId':deptId});
              ProjectPlan.find({'where':{'and':finalData}},function(err,planData){
                var finalObject=[];
                if(planData.length>0){
                  for(var i=0;i<planData.length;i++){
                    var findStatus=false;
                    if(planData[i].finalStatus!=undefined && planData[i].finalStatus!=null && planData[i].finalStatus== "Approval"){
                      for(j=0;j<budgetData.length;j++){
                        if(budgetData[j].finalStatus!=undefined && budgetData[j].finalStatus!=null && budgetData[j].finalStatus!= "Rejected"){
                          if(budgetData[j].planDetails!=undefined && budgetData[j].planDetails!=null && budgetData[j].planDetails.length>0){
                            var planDetails=budgetData[j].planDetails;
                            for(var x=0;x<planDetails.length;x++){
                              if(planDetails[x].planId==planData[i].planId){
                                findStatus=true;
                                break;
                              }
                            }
                          }
                        }
                        if(findStatus){
                          break;
                        }
                      }
                      if(!findStatus){
                        finalObject.push(planData[i]);
                      }
                    }

                  }
                  cb(null,finalObject);
                }else{
                  cb(null,finalObject);
                }
              })
            }
          }
        })
      }else{
          taskStatus.find({},function(err,taskData){
            if(taskData.length>0) {
              var taskArray = [];
              for (var i = 0; i < taskData.length; i++) {
                if (taskData[i].name == 'new' || taskData[i].name == 'pending' || taskData[i].name == 'working') {
                  taskArray.push({"taskStatusId": taskData[i].id + ''});
                }
              }
              if (deptId == 'all') {
                var finalData=[];
                finalData.push({'ulb':ulb});
                finalData.push({'financialYear':fYear});
                finalData.push({'or':taskArray});
                console.log('details before '+JSON.stringify(finalData));
                ProjectPlan.find({'where': {'and': finalData}}, function (err, planData) {
                  var finalObject = [];
                  if (planData!=null && planData.length > 0) {
                    cb(null, planData);
                  } else {
                    cb(null, finalObject);
                  }
                })
              }else{

                var finalData=[];
                finalData.push({'ulb':ulb});
                finalData.push({'financialYear':fYear});
                finalData.push({'or':taskArray});
                finalData.push({'departmentId':deptId});
                ProjectPlan.find({'where':{'and':finalData}},function(err,planData){
                    var finalObject=[];
                    if(planData.length>0){
                      cb(null,planData);
                    }else{
                      cb(null,finalObject);
                    }
                })
              }
            }
          })
        }


    });
  };

  Departmentwisebudget.remoteMethod('getBudgetPlans', {
    description: "Send Valid ulb",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'ulb', type: 'string', http: {source: 'query'}},
      {arg: 'department', type: 'string', http: {source: 'query'}},
      {arg: 'fYear', type: 'string', http: {source: 'query'}}],
    http: {
      path: '/getBudgetPlans',
      verb: 'GET'
    }
  });



};

