var server = require('../../server/server');
module.exports = function(Citizen) {

  Citizen.validatesUniquenessOf('email', {message: 'Email could be unique'});
  Citizen.validatesUniquenessOf('employeeId', {message: 'Email could be unique'});
  Citizen.observe('before save', function (ctx, next) {
    if(ctx.data!=undefined && ctx.data!=null){
      ctx.data['updatedTime']=new Date();
      next();
    }else {
      ctx.instance.createdTime=new Date();
      next();
    }
  });
  Citizen.remoteMethod('forgotPassword', {
    description: "Send Valid Details",
    returns: {
      arg: 'data',
      type: "object"
    },
    accepts: [{arg: 'searchData', type: 'object', http: {source: 'query'}}],
    http: {
      path: '/forgotPassword',
      verb: 'GET'
    }
  });

  Citizen.forgotPassword = function (data, cb) {
    Citizen.findOne({'where':{'email':data.emailId}},function (err, email) {
    if(err)console.log('error message is '+err);
    if(email!=undefined && email!=null){
        var randomstring = require("randomstring");
        var uniqueId = randomstring.generate({
          length: 6,
          charset: 'alphanumeric'
        });
        var otp = uniqueId;
        var otpStatus = false;
        var updateData={
          password:otp,
          confirmPassword:otp
        };
        email.updateAttributes(updateData,function (err,updated) {
          var EmailTemplete = server.models.EmailTemplete;
          var Sms = server.models.Sms;
          EmailTemplete.find({"where":{"emailType": "scheme"}},function (err, emailTemplete) {
            if(emailTemplete!=null &&emailTemplete.length>0) {
              var Dhanbademail = server.models.DhanbadEmail;
              var message =emailTemplete[0].citizenText+' '+otp;
              Dhanbademail.create({
                "to":  data.emailId,
                "subject": emailTemplete[0].citizenSubject,
                "text":message
              }, function (err, email) {
                console.log(email);
              });
              if(emailTemplete[0].citizenSMS && email.phone) {
                var smsData = {
                  "message": emailTemplete[0].citizenSMS + " " + otp,
                  "mobileNo": email.phone,
                  "smsservicetype": "singlemsg"
                };
                Sms.create(smsData, function (err, smsInfo) {
                });
              }
              cb(null,updated);
            }else{
              cb(null,updated);
            }
          });

        });
    }
    else{
      var message={
        message:'Please Register With US'
      };
      cb(null, message);
    }
    });
  };

  Citizen.observe('after save', function (ctx, next) {
    var Citizen = server.models.Citizen;
    if(ctx.isNewInstance){
      var Emailtemplete = server.models.EmailTemplete;
      Emailtemplete.find({"where":{"emailType": "scheme"}},function (err, emailTemplete) {
        if(emailTemplete!=null && emailTemplete.length>0){
          var Dhanbademail = server.models.DhanbadEmail;
          Dhanbademail.create({
            "to": ctx.instance.email,
            "subject": emailTemplete[0].registerSubject,
            "text": emailTemplete[0].registerText
          }, function (err, email) {
            console.log(email);
          });
          if(emailTemplete[0].registerSMS && ctx.instance.phone) {
            console.log('before sending sms');
            var Sms = server.models.Sms;
            var smsData = {
              "message": emailTemplete[0].registerSMS,
              "mobileNo": ctx.instance.phone,
              "smsservicetype": "singlemsg"
            };
            Sms.create(smsData, function (err, smsInfo) {
            });
          }
          next();
        }else{
          next();
        }
      });
    } else {
      next();
    }
  });

};
