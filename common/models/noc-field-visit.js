var server = require('../../server/server');
module.exports = function(Nocfieldvisit) {
  Nocfieldvisit.observe('before save', function (ctx, next) {
    if(ctx.instance!=undefined && ctx.instance!=null){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      WorkflowForm.findOne({"where": {"schemeUniqueId": 'citizenNOCRequestVisit'}}, function (err, workflowForm) {
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo
                  }
                  workflowData.push(flowData);
                }
              }
              ctx.instance.workflow = workflowData;
              ctx.instance.acceptLevel = 0;
              ctx.instance.finalStatus = false;
              ctx.instance.requestStatus = false;
              ctx.instance.filedStatus = false;
              ctx.instance.workflowId = workflowForm.workflowId;
              ctx.instance.maxlevel = flowDataList.maxLevel;
              ctx.instance.createdTime = new Date();
              next();
            });

          });
        }
      });
    }else{
      next();
    }

  });


  Nocfieldvisit.observe('loaded', function (ctx, next) {

    if(ctx.instance){

            if(ctx.instance.workflowId!=undefined && ctx.instance.workflowId!=null && ctx.instance.workflowId!='' ){
              var WorkflowEmployees=server.models.WorkflowEmployees;
              WorkflowEmployees.find({'where':{'and':[{'workflowId':ctx.instance.workflowId},{"status": "Active"}]}}, function (err, workflowEmployeeList) {
                if(workflowEmployeeList!=null && workflowEmployeeList.length>0){
                  ctx.instance.workflowData=workflowEmployeeList;
                  next();
                }else{
                  next();
                }
              })
            }else{
              next();
            }

    }else{
      next();
    }
  });

  Nocfieldvisit.getDetails = function (employee,planId, cb) {

    Nocfieldvisit.find({"where":{"nocId":planId}},function(err, fieldVisitData){
      var fieldVisitList=[];
      if(fieldVisitData!=null && fieldVisitData.length>0){

        var Employee=server.models.Employee;
        var adminStatus=false;
        Employee.find({'where':{"employeeId":employee}}, function (err, employeeList) {
          if(employeeList!=null && employeeList.length>0) {
            var firstEmployeeDetails = employeeList[0];
            if (firstEmployeeDetails.role == 'superAdmin' || firstEmployeeDetails.role == 'projectAdmin') {
              adminStatus = true;
            }
            for (var i = 0; i < fieldVisitData.length; i++) {
              var data = fieldVisitData[i];
              var workflowDetails = data.workflowData;
              var accessFiledVisit = false;
              var approveFiledVisit = false;
              if (workflowDetails != undefined && workflowDetails != null && workflowDetails.length > 0) {
                var acceptLevel = data.acceptLevel;
                var editDetails;
                if (data.createdPersonId == employee) {
                  editDetails = true;
                }
                var availableStatus = false;
                if (data.finalStatus == false) {
                  for (var j = 0; j < workflowDetails.length; j++) {
                    var availableStatusLevel = false;
                    if (workflowDetails[j].employees != undefined && workflowDetails[j].employees != null && workflowDetails[j].employees.length > 0) {
                      var employeeList = workflowDetails[j].employees;
                      if (employeeList != undefined && employeeList != null && employeeList.length > 0) {
                        for (var x = 0; x < employeeList.length; x++) {
                          if (employeeList[x] == employee) {
                            availableStatus = true;
                            availableStatusLevel = true;
                            break;
                          }
                        }
                      }
                      if (availableStatusLevel) {
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }else if(adminStatus){
                        availableStatus=true;
                        var workFlowLevel = parseInt(workflowDetails[j].levelNo);
                        if (acceptLevel == (workFlowLevel - 1)) {
                          approveFiledVisit = true;
                        }
                      }

                    }

                  }
                  if (availableStatus || editDetails ) {
                    if (editDetails) {
                      if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                        data.editDetails = true;
                      } else {
                        data.editDetails = false;
                      }
                    }
                    data.approveFiledVisit = approveFiledVisit;
                    data.availableStatus = availableStatus;
                    fieldVisitList.push(data);
                  }
                }else{
                  if (editDetails) {
                    if (acceptLevel == 0 || data.finalStatus == 'Rejected') {
                      data.editDetails = true;
                    } else {
                      data.editDetails = false;
                    }
                  }
                  data.approveFiledVisit = false;
                  data.availableStatus = true;
                  fieldVisitList.push(data);
                }


              }
            }
            cb(null,fieldVisitList);
          }else{
            cb(null,fieldVisitList);
          }
        });

      }else{
        cb(null,fieldVisitList);
      }
    });
  };


  Nocfieldvisit.remoteMethod('getDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'employeeId', type: 'string', http: {source: 'query'},required:true},
      {arg: 'nocId', type: 'string', http: {source: 'query'},required:true}],
    http: {
      path: '/getDetails',
      verb: 'GET'
    }
  });


  Nocfieldvisit.updateDetails = function (approvalDetails, cb) {
    Nocfieldvisit.findOne({'where':{'id':approvalDetails.requestId}},function(err, fieldVisitDetails){

      if(fieldVisitDetails!=undefined && fieldVisitDetails!=null){
        var finalObject={};
        var workflowDetails=fieldVisitDetails.workflow;
        if(workflowDetails!=undefined && workflowDetails!=null && workflowDetails.length>0){
          var workflowData=[];
          for(var  i=0;i< workflowDetails.length;i++){
            var data=workflowDetails[i];
            var workFlowLevel=parseInt(workflowDetails[i].levelNo);
            if((approvalDetails.acceptLevel+1)==workFlowLevel){
              data.approvalStatus=approvalDetails.requestStatus;
              data.employeeName=approvalDetails.employeeName;
              data.employeeId=approvalDetails.employeeId;
              data.comment=approvalDetails.comment;
              data.date=new Date();
            }
            workflowData.push(data);

          }
          finalObject.workflow=workflowData;
        }
       if(approvalDetails.requestStatus=="Approval"){
          if(fieldVisitDetails.maxlevel==(approvalDetails.acceptLevel+1)){
            finalObject.finalStatus=approvalDetails.requestStatus;
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;

          }else{
            finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          }

        } else if(approvalDetails.requestStatus=="Rejected"){
          finalObject.finalStatus=approvalDetails.requestStatus;
          finalObject.acceptLevel=approvalDetails.acceptLevel+1;
          finalObject.rejectComment=approvalDetails.comment;
        }
        var Sms = server.models.Sms;
        fieldVisitDetails.updateAttributes(finalObject, function(err, finalData){
          if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Rejected" ){
            var NocRequest=server.models.NocRequest;
            NocRequest.findOne({'where':{}}, function (err, nocRequest) {
              if(nocRequest!=undefined && nocRequest!=null && nocRequest!=''){
                var objectDetails= {
                  "filedStatus":finalObject.finalStatus,
                  "finalStatus":finalObject.finalStatus
                }
                    nocRequest.updateAttributes(objectDetails);
                if(finalData.createdPersonId!=undefined && finalData.createdPersonId!=null && finalData.createdPersonId!='' ){
                  var Employee=server.models.Employee;
                  var EmailTemplete = server.models.EmailTemplete;
                  Employee.find({'where':{'employeeId':finalData.createdPersonId}}, function (err, employeeDetails) {
                    if(employeeDetails!=null && employeeDetails.length>0){
                      EmailTemplete.findOne({"where":{"emailType": "projectWardWorks"}},function (err, emailTemplete) {
                        if(emailTemplete!=null ) {
                          var nodemailer = require('nodemailer');
                          var smtpTransport = require('nodemailer-smtp-transport');
                          var transporter = nodemailer.createTransport(smtpTransport({
                            host: "smtp.gmail.com",
                            port: 465,
                            secure: true,
                            auth: {
                              user: emailTemplete.emailId,
                              pass: 'admin@admin'
                            }
                          }));
                          var message = emailTemplete.fieldVisitRejectedMessage + '<br>Reason is ' + approvalDetails.comment;
                          console.log('after reject' + message);
                          var mailOptions = {
                            from: emailTemplete.emailId,
                            to: employeeDetails[0].email,
                            subject: emailTemplete.fieldVisitRejectedEmail, // Subject line
                            text: '', // plaintext body
                            html: message // html body
                          };
                          transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                              return console.log(error);
                            }
                          });
                          if(emailTemplete.fieldVisitRejectedSMS && employeeDetails[0].mobile) {
                            var smsData = {
                              "message": emailTemplete.fieldVisitRejectedSMS,
                              "mobileNo": employeeDetails[0].mobile,
                              "smsservicetype": "singlemsg"
                            };

                            Sms.create(smsData, function (err, smsInfo) {
                            });
                          }
                        }
                      });
                      cb(null, finalData);
                    }else{
                      cb(null, finalData);
                    }
                  });

                }
                else{
                  cb(null, finalData);
                }
                }else{
                cb(null,finalData);
              }


            });


          }else if(finalObject.finalStatus!=undefined && finalObject.finalStatus!=null && finalObject.finalStatus=="Approval"){
            var NocRequest=server.models.NocRequest;
            NocRequest.findOne({'where':{}}, function (err, nocRequest) {
              if (nocRequest != undefined && nocRequest != null && nocRequest != '') {
                var objectDetails = {
                  "filedStatus": finalObject.finalStatus,
                  "finalStatus": finalObject.finalStatus
                }
                nocRequest.updateAttributes(objectDetails);
                if (finalData.createdPersonId != undefined && finalData.createdPersonId != null && finalData.createdPersonId != '') {
                  var Employee = server.models.Employee;
                  var EmailTemplete = server.models.EmailTemplete;
                  Employee.find({'where': {'employeeId': finalData.createdPersonId}}, function (err, employeeDetails) {
                    if (employeeDetails != null && employeeDetails.length > 0) {
                      EmailTemplete.findOne({"where": {"emailType": "projectWardWorks"}}, function (err, emailTemplete) {
                        if (emailTemplete != null) {
                          var nodemailer = require('nodemailer');
                          var smtpTransport = require('nodemailer-smtp-transport');
                          var transporter = nodemailer.createTransport(smtpTransport({
                            host: "smtp.gmail.com",
                            port: 465,
                            secure: true,
                            auth: {
                              user: emailTemplete.emailId,
                              pass: 'admin@admin'
                            }
                          }));
                          var message = emailTemplete.fieldVisitRequestApprovalMessage + '<br>Reason is ' + approvalDetails.comment;
                          var mailOptions = {
                            from: emailTemplete.emailId,
                            to: employeeDetails[0].email,
                            subject: emailTemplete.fieldVisitRequestApprovalEmail, // Subject line
                            text: '', // plaintext body
                            html: message // html body
                          };
                          transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                              return console.log(error);
                            }
                            console.log('Message sent: ' + info.response);
                          });
                          if (emailTemplete.fieldVisitRequestApprovalSMS && employeeDetails[0].mobile) {
                            var smsData = {
                              "message": emailTemplete.fieldVisitRequestApprovalSMS,
                              "mobileNo": employeeDetails[0].mobile,
                              "smsservicetype": "singlemsg"
                            };

                            Sms.create(smsData, function (err, smsInfo) {
                              console.log('SMS info:' + JSON.stringify(smsInfo));
                            });
                          }
                        }
                      });
                      cb(null, finalData);
                    } else {
                      cb(null, finalData);
                    }
                  });
                }
              } else {
                cb(null, finalData);
              }
            });
          }else{
            cb(null, finalData);
          }
        });
      }else {
        var message = 'Please provide valid details';
        cb(message,null);
      }

    });


  };

  Nocfieldvisit.remoteMethod('updateDetails', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateDetails',
      verb: 'POST'
    }
  });

  Nocfieldvisit.updateContent= function (updatedDetails, cb) {
    Nocfieldvisit.findOne({'where':{'id':updatedDetails.id}},function(err, fieldVisitDetails){
      var WorkflowForm=server.models.WorkflowForm;
      var Workflow=server.models.Workflow;
      var WorkflowEmployees=server.models.WorkflowEmployees;
      WorkflowForm.findOne({"where": {"and":[{"status": "Active"},{"schemeUniqueId": "fieldVisit"}]}}, function (err, workflowForm) {
        console.log('work form '+JSON.stringify(workflowForm));
        if(workflowForm!=undefined && workflowForm!=null ){
          Workflow.findOne({'where':{'workflowId':workflowForm.workflowId}},function (err, flowDataList) {
            WorkflowEmployees.find({'where':{'workflowId':workflowForm.workflowId}},function (err, listData) {
              var workflowData = [];
              if (listData != undefined && listData != null && listData.length > 0) {
                for (var i = 0; i < listData.length; i++) {
                  var flowData = {
                    workflowId: listData[i].workflowId,
                    levelId: listData[i].levelId,
                    status: listData[i].status,
                    maxLevels: listData[i].maxLevels,
                    levelNo: listData[i].levelNo

                  }
                  workflowData.push(flowData);
                }
              }
              updatedDetails.workflow = workflowData;
              updatedDetails.workflowId=workflowForm.workflowId;
              updatedDetails.maxlevel=flowDataList.maxLevel;
              updatedDetails.acceptLevel=0;
              updatedDetails.finalStatus=false;
              updatedDetails['updatedTime']=new Date();
              fieldVisitDetails.updateAttributes(updatedDetails,function(err,data){
              });
              cb(null, fieldVisitDetails);
            });

          });
        }
      });
    });
  };

  Nocfieldvisit.remoteMethod('updateContent', {
    description: "Send Valid EmployeeId",
    returns: {
      arg: 'data',
      type: "object",
      root:true
    },
    accepts: [{arg: 'approval', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateContent',
      verb: 'POST'
    }
  });




};
