var server = require('../../server/server');
  module.exports = function(Projectclosure) {
    Projectclosure.observe('before save', function (ctx, next) {
      if (ctx.instance != undefined && ctx.instance != null) {

         if(ctx.instance.departmentId!=undefined && ctx.instance.departmentId!=null && ctx.instance.departmentId!=''
          && ctx.instance.assetName!=undefined && ctx.instance.assetName!=null && ctx.instance.assetName!=''){
          var AssetForLand=server.models.AssetForLand;
         var details={
           "assetType":"land",
           "assetName":ctx.instance.assetName,
           "contactPersonDepartment":ctx.instance.departmentId,
           "landDetails":ctx.instance.landDetails,
           "areaInUnits":ctx.instance.areaInUnits,
           "locality":ctx.instance.landLocality,
           "planId" : ctx.instance.planId,
         }
          AssetForLand.create(details,function(err, assetdetails){
           if( assetdetails!=null){
              ctx.instance['assetNumber']=assetdetails.assetNumber;
            }
            if(ctx.instance.planId!=undefined && ctx.instance.planId!=null ) {
              var ProjectTasks = server.models.ProjectTasks;
              var ProjectCharter = server.models.ProjectCharter;
              var ProjectPlan = server.models.ProjectPlan;
              var TaskStatus = server.models.TaskStatus;
              var planId = ctx.instance.planId;
              TaskStatus.findOne({'where': {'name': 'closed'}}, function (err, taskStatus) {
                ProjectCharter.findOne({'where': {'projectId': planId}}, function (err, projectcharter) {
                  if (err)
                    console.log('Error message is' + err);
                  var object = {
                    "status": "closed"
                  }
                  projectcharter.updateAttributes(object, function (err, updatedcharter) {
                  });
                  ProjectPlan.findOne({'where': {'planId': planId}}, function (err, planDetailsList) {
                    if (err)
                      console.log('Error message is' + err);
                    var object1 = {
                      "taskStatusId": taskStatus.id
                    }
                    planDetailsList.updateAttributes(object1, function (err, updatedProjectPlan) {
                    });
                  });
                  ProjectTasks.find({'where': {'planId': planId}}, function (err, taskDetailsList) {
                    if (err)
                      console.log('Error message is' + err);
                    if (taskDetailsList != undefined && taskDetailsList != null && taskDetailsList.length > 0) {
                      var taskList = taskDetailsList;
                      for (var i = 0; i < taskList.length; i++) {
                        var projecTask = taskList[i];
                        var taskList = [];
                        if (projecTask.taskList != null && projecTask.taskList.length > 0) {
                          for (var j = 0; j < projecTask.taskList.length; j++) {
                            var subTask = projecTask.taskList[j];
                            if (subTask.status == 'Completed') {
                              taskList.push(subTask);
                            } else {
                              subTask.status = "closed"
                              taskList.push(subTask);
                            }
                          }
                        }
                        var object = {
                          "taskList": taskList
                        }
                        projecTask.updateAttributes(object, function (err, updatedProjectTasks) {
                        });
                      }
                    }
                  });
                });
              });
            }
            ctx.instance.createdTime = new Date();
            next();
          });

        }else{
          if(ctx.instance.planId!=undefined && ctx.instance.planId!=null ){
            var ProjectTasks = server.models.ProjectTasks;
            var ProjectCharter=server.models.ProjectCharter;
            var ProjectPlan=server.models.ProjectPlan;
            var TaskStatus=server.models.TaskStatus;
            var planId=ctx.instance.planId;
            TaskStatus.findOne({'where':{'name':'closed'}}, function (err, taskStatus) {
              ProjectCharter.findOne({'where':{'projectId':planId}},function (err, projectcharter) {
                if(err)
                  console.log('Error message is'+err);
                var object={
                  "status":"closed"
                }
                projectcharter.updateAttributes(object,function (err, updatedcharter) {
                });
                ProjectPlan.findOne({'where':{'planId':planId}},function (err, planDetailsList) {
                  if(err)
                    console.log('Error message is'+err);
                  var object1={
                    "taskStatusId":taskStatus.id
                  }
                  planDetailsList.updateAttributes(object1,function (err, updatedProjectPlan) {

                  });
                });
                ProjectTasks.find({'where':{'planId':planId}},function (err, taskDetailsList) {
                  if(err)
                    console.log('Error message is'+err);
                  if(taskDetailsList!=undefined && taskDetailsList!=null && taskDetailsList.length>0){
                    var taskList=taskDetailsList;
                    for(var i=0;i<taskList.length;i++){
                      var projecTask=taskList[i];
                      var taskList=[];
                      if(projecTask.taskList!=null && projecTask.taskList.length>0){
                        for(var j=0;j<projecTask.taskList.length;j++){
                          var subTask=projecTask.taskList[j];
                          if(subTask.status =='Completed'){
                            taskList.push(subTask);
                          }else{
                            subTask.status="closed"
                            taskList.push(subTask);
                          }
                        }
                      }
                      var object={
                        "taskList":taskList
                      }
                      projecTask.updateAttributes(object,function (err, updatedProjectTasks) {
                      });
                    }
                  }
                });
              });
            });
          }
          ctx.instance.createdTime = new Date();
          next();
        }
      } else {
        ctx.data.updatedTime = new Date();
        next();
      }
    });
};
